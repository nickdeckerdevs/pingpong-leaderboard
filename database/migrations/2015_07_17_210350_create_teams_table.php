<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('player_one')->unsigned();
            $table->foreign('player_one')->references('id')->on('players');
            $table->integer('player_two')->unsigned();
            $table->foreign('player_two')->references('id')->on('players')->nullable();
            $table->text('team_name')->nullable();
            $table->integer('wins')->nullable();
            $table->integer('losses')->nullable();
            $table->integer('ties')->nullable();
            $table->integer('forfeits')->nullable();
            $table->integer('disqualified')->nullable();
            $table->text('record')->nullable();
            $table->integer('win_point_differental')->nullable();
            $table->integer('loss_point_differental')->nullable();
            $table->integer('average_win_point_differental')->nullable();
            $table->integer('average_loss_point_differental')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('teams');
    }
}
