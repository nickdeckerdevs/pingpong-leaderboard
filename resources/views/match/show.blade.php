<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                @endif
                @if(isset($errors) && isset($errors->bags))
                    ERRORS :: THEY ARE THERE
                @endif
                
                
                @if($edit == true)
                
                
                
                {!! Form::model($match, ['action' => 'MatchController@update', 'method' => 'PATCH']) !!}
                    {!! Form::hidden('id') !!}
                    {!! Form::label('team_one', 'Team One') !!}
                    {{ Team::find($match->team_one)->team_name }}
                    {!! Form::label('team_two', 'Team Two') !!}
                    {{ Team::find($match->team_two)->team_name }}
                    {!! Form::label('team_one_score', 'Team One Score') !!}
                    {!! Form::text('team_one_score') !!}
                    {!! Form::label('team_two_score', 'Team Two Score') !!}
                    {!! Form::text('team_two_score') !!}
                    
                    {!! Form::label('winner', 'Winner') !!}
                    {!! Form::select('winner', $teams) !!}
                    
                    {!! Form::label('loser', 'Loser') !!}
                    {!! Form::select('loser', $teams) !!}
                    
                    {!! Form::label('forfeit', 'Forfeit') !!}
                    {!! Form::select('forfeit', $teams) !!}
                    
                    {!! Form::label('disqualified', 'Disqualified') !!}
                    {!! Form::select('disqualified', $teams) !!}
                    
                    {!! Form::label('tie', 'Tie') !!}
                    {!! Form::checkbox('tie') !!}
                    
                    {!! Form::label('bye', 'Bye') !!}
                    {!! Form::checkbox('Bye') !!}
                    
                    {!! Form::submit('Save Match') !!}
                {!! Form::close() !!}
                
                @else
                <table>
                    <tr>
                        <td>{{ $match->id }}</td>
                        <td>{{ $match->teamOne->name }}</td>
                        <td>{{ $match->teamTwo->name }}</td>
                        <td>{{ $match->team_one_score }}</td>
                        <td>{{ $match->team_two_score }}</td>
                        <td>{{ $match->winner }}</td>
                        <td>{{ $match->loser }}</td>
                        <td>{{ $match->forfeit }}</td>
                        <td>{{ $match->disqualified }}</td>
                        <td>{{ $match->tie }}</td>
                        <td>{{ $match->bye }}</td>
                        <td>{{ $match->point_difference }}</td>
                        <td>{{ $match->created_at }}</td>
                        <td>{{ $match->updated_at }}</td>
                    </tr>
                </table>
                {!! link_to_route('team.edit', 'Edit Team', $team->id ) !!}
                
                @endif
                {!! link_to_route('team.index', 'Show All Teams') !!}
                
            </div>
        </div>
    </body>
</html>
