<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Leaderboard 1.0</div>
                <div class="nav-menu">
                    <h2>{!! link_to_route('player.index', 'Players') !!}</h2>
                    <h2>{!! link_to_route('team.index', 'Teams') !!}</h2>
                    <ul class="navigation">
                        <li>Players
                            <ul class="subnav">
                                <li>{!! link_to_route('player.create', 'Create Player') !!}</li>
                                <li>{!! link_to_route('player.create', 'Create Player') !!}</li>
                                <li>    </li>
                            </ul>
                        </li>
                        <li>Teams
                        <ul class="subnav">
                                <li>Create Player</li>
                                <li>Edit Players</li>
                                <li>    </li>
                        </ul>
                        </li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
            </div>
        </div>
    </body>
</html>
