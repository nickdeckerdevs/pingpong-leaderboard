<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        
        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <navigation class="nav">
            <ul>
                <li>{!! link_to_route('player.index', 'Players') !!}</li>
                <li>{!! link_to_route('team.index', 'Teams') !!}</li>
                <li>{!! link_to_route('match.index', 'Matches') !!}</li>
            </ul>
        </navigation>
        <div class="container">
            <div class="content">
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                @endif
                @foreach($matches as $match)
                <h2>{!! link_to_route('match.show', 'See '.$match->teamOne->team_name.' vs '.$match->teamTwo->team_name, $match->id) !!}</h2> 
                
                @endforeach
                {!! link_to_route('match.create', 'Create A New Match') !!}
            </div>
        </div>
    </body>
</html>
