<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $table = 'players';
    protected $fillable = ['name', 'nickname'];
    
    public function getName() {
        return $this->name;
    }
    
    public function getNickName() {
        return $this->nickname;
    }
    
}
