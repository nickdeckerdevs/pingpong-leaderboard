<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team;
use App\Player;
use App\Match;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Validator;
use Redirect;

class MatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $matches = Match::all();
        return view('match.index', compact('matches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $teams = ['' => 'Select One'] + Team::lists('team_name', 'id')->toArray();
        return view('match.create', compact('teams'));
    }
    
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            [
                'team_one' => Input::get('team_one'),
                'team_two' => Input::get('team_two')
            ],
            [
                'team_one' => 'required',
                'team_two' => 'required'
            ]
        );
        if($validator->fails()) {
            var_dump(Input::all());
            dd($validator->errors()->all());
            return redirect()->back()->withErrors($validator->errors()->all());
        }
        /* 
         * This defaults to 9 so we can get past foreign key issues. 9 is the 
         * team id that is used for no other team.  Their stats will be for the 
         * garbage 
         */
        $match = Match::create([
            'team_one' => Input::get('team_one'),
            'team_two' => Input::get('team_two'),
            'winner' => 9,
            'loser' => 9,
            'forfeit' => 9,
            'disqualified' => 9
        ]);
        return Redirect::route('match.index')->with('message', 'Your match has been created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $edit = false;
        $match = Match::find($id);
        return view('match.show', compact('match', 'edit'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $edit = true;
        $match = Match::find($id);
        return view('match.show', compact('match', 'edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
