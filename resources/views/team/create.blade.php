<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                
                {!! Form::open(['action' => 'TeamController@store']) !!}
                    {!! Form::label('player_one', 'Player One') !!}
                    {!! Form::select('player_one', $players) !!}
                    {!! Form::label('player_two', 'Player Two') !!}
                    {!! Form::select('player_two', $players) !!}
                    {!! Form::label('team_name', 'Team Name') !!}
                    {!! Form::text('team_name') !!}
                    {!! Form::submit('Create Team') !!}
                {!! Form::close() !!}
            </div>
        </div>
    </body>
</html>
