<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team;
use App\Player;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Validator;
use Redirect;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $teams = Team::all();
        return view('team.index', compact('teams'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $players = ['' => 'Select One'] + Player::lists('name', 'id')->toArray();
        return view('team.create', compact('players'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make(
            [
                'player_one' => Input::get('player_one'),
                'player_two' => Input::get('player_two'),
                'team_name' => Input::get('team_name')
            ],
            [
                'player_one' => 'required',
                'team_name' => 'required|unique:teams'
            ]
        );
        
        if($validator->fails()) {
            var_dump(Input::all());
            dd($validator->errors()->all());
            return redirect()->back()->withErrors($validator->errors()->all());
        }
        
        if(Input::get('player_two') == '') {
            $newTeam = Team::create([
                'player_one' => Input::get('player_one'),
                
                'player_two' => null,
                'team_name' => Input::get('team_name')
            ]);
        } else {
            $newTeam = Team::create([
                'player_one' => Input::get('player_one'),
                'player_two' => Input::get('player_two'),
                'team_name' => Input::get('team_name')
            ]);
        }
        return Redirect::route('team.index')->with('message', $newTeam->team_name.' has been created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $edit = false;
        $team = Team::find($id);
        return view('team.show', compact('team', 'edit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $edit = true;
        $team = Team::find($id);
        $players = ['' => 'Select One'] + Player::lists('name', 'id')->toArray();
        return view('team.show', compact('team', 'edit', 'players'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request)
    {
        $input = array_except(Input::all(), '_method');
        $team = Team::find(Input::get('id'));
        $team->update($input);
        return Redirect::route('team.index')->with('message', $team->team_name.' has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
