<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('team_one')->unsigned();
            $table->foreign('team_one')->references('id')->on('teams');
            $table->integer('team_one_score')->nullable();
            $table->integer('team_two')->unsigned();
            $table->foreign('team_two')->references('id')->on('teams')->nullable();
            $table->integer('team_two_score')->nullable();
            $table->integer('winner')->unsigned();
            $table->foreign('winner')->references('id')->on('teams')->nullable();
            $table->integer('loser')->unsigned();
            $table->foreign('loser')->references('id')->on('teams')->nullable();
            $table->integer('forfeit')->unsigned();
            $table->foreign('forfeit')->references('id')->on('teams')->nullable();
            $table->integer('disqualified')->unsigned();
            $table->foreign('disqualified')->references('id')->on('teams')->nullable();
            $table->boolean('tie')->default(0);
            $table->boolean('bye')->default(0);
            $table->integer('point_difference')->default(0);
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('matches');
    }
}
