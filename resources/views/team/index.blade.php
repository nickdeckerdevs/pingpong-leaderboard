<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        
        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <navigation class="nav">
            <ul>
                <li>{!! link_to_route('player.index', 'Players') !!}</li>
                <li>{!! link_to_route('team.index', 'Teams') !!}</li>
            </ul>
        </navigation>
        <div class="container">
            <div class="content">
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                @endif
                @foreach($teams as $team)
                <h2>{!! link_to_route('team.show', 'See '.$team->team_name, $team->id) !!}</h2> 
                <p>[ {!! link_to_route('player.show', 'See '.$team->playerOne->name, $team->player_one) !!}
                    @if(isset($team->player_two))
                    , {!! link_to_route('player.show', 'See '.$team->playerTwo->name, $team->player_two) !!}
                    @endif]</p>
                @endforeach
                {!! link_to_route('team.create', 'Create A New Team') !!}
            </div>
        </div>
    </body>
</html>
