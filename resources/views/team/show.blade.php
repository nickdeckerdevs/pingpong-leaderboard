<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                @endif
                @if(isset($errors))
                    {{ $team->getName() }}
                @endif
                @if($edit == true)
                
                {!! Form::model($team, ['action' => 'TeamController@update', 'method' => 'PATCH']) !!}
                    {!! Form::hidden('id') !!}
                    {!! Form::label('player_one', 'Player One') !!}
                    {!! Form::select('player_one', $players) !!}
                    {!! Form::label('player_two', 'Player Two') !!}
                    {!! Form::select('player_two', $players) !!}
                    {!! Form::label('team_name', 'Team Name') !!}
                    {!! Form::text('team_name') !!}
                    {!! Form::submit('Save Team') !!}
                {!! Form::close() !!}
                
                @else
                <table>
                    <tr>
                        <td>{{ $team->id }}</td>
                        <td>{{ $team->playerOne->name }}</td>
                        <td>{{ $team->playerTwo->name }}</td>
                        <td>{{ $team->team_name }}</td>
                        <td>{{ $team->created_at }}</td>
                        <td>{{ $team->updated_at }}</td>
                    </tr>
                </table>
                {!! link_to_route('team.edit', 'Edit Team', $team->id ) !!}
                
                @endif
                {!! link_to_route('team.index', 'Show All Teams') !!}
                
            </div>
        </div>
    </body>
</html>
