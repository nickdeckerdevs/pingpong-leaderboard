<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                @endif
                @if(isset($errors))
                    {{ $player->getNickName() }}
                @endif
                @if($edit == true)
                
                {!! Form::model($player, ['action' => 'PlayerController@update', 'method' => 'PATCH']) !!}
                    {!! Form::hidden('id') !!}
                    {!! Form::label('name', 'Name') !!}
                    {!! Form::text('name') !!}
                    {!! Form::label('nickname', 'Nickname') !!}
                    {!! Form::text('nickname') !!}
                    {!! Form::submit('Save Player') !!}
                {!! Form::close() !!}
                
                @else
                <table>
                    <tr>
                        <td>{{ $player->id }}</td>
                        <td>{{ $player->name }}</td>
                        <td>{{ $player->nickname }}</td>
                        <td>{{ $player->image }}</td>
                        <td>{{ $player->created_at }}</td>
                        <td>{{ $player->updated_at }}</td>
                    </tr>
                </table>
                {!! link_to_route('player.edit', 'Edit Player', $player->id ) !!}
                
                @endif
                {!! link_to_route('player.index', 'Show All Players') !!}
                
            </div>
        </div>
    </body>
</html>
