<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        
        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <navigation class="nav">
            <ul>
                <li>{!! link_to_route('player.index', 'Players') !!}</li>
                <li>{!! link_to_route('team.index', 'Teams') !!}</li>
                <!--<li> link_to_route('match.index', 'Matches') !!}</li>-->
            </ul>
        </navigation>
        <div class="container">
            <div class="content">
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                @endif
                @foreach($players as $player)
                <p style="font-size: 2em;">{{ $player->name }} [ <a href="{{ route('player.show', $player->id) }}">{{ $player->nickname }}</a> ]</p>
                <p><a href="{{ route('player.edit', [$player->id]) }}">Edit {{ $player->name }}</a>
                @endforeach
                <p> {!! link_to_route('player.create', 'Create A New Player') !!}
            </div>
        </div>
    </body>
</html>
