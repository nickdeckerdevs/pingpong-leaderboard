<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Player;

class Team extends Model
{
    protected $table = 'teams';
    protected $fillable = ['player_one', 'player_two', 'team_name'];
    
    public function getName() {
        return $this->name;
    }
    
    public function playerOne() {
        return $this->hasOne('App\Player', 'id', 'player_one');
    }
    
    public function playerTwo() {
        return $this->hasOne('App\Player', 'id', 'player_two');
    }
    
    public function getPlayerOne() {
        
        return Player::find($this->player_one);
    }
    
    public function getPlayerTwo() {
        return Player::find($this->player_two);
//        return isset($this->player_two) ? $this->player_two : '[ No Teammate ]';
    }
}
