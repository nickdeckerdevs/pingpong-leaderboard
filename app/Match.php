<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Player;
use Team;

class Match extends Model
{
    protected $table = 'matches';
    protected $fillable = ['team_one', 'team_two', 'team_one_score', 'team_two_score', 'winner', 'loser', 'forfeit', 'disqualified', 'tie', 'bye'];
    
    public function teamOne() {
        return $this->hasOne('App\Team', 'id', 'team_one');
    }
    
    public function teamTwo() {
        return $this->hasOne('App\Team', 'id', 'team_two');
    }
}
