<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call('playerSeeder');

        Model::reguard();
    }
}

use App\Player;
class playerSeeder extends Seeder {
    public function run() {
        $players = [ 
            'Nicholas Decker' => 'The Deacon',
            'Jessica Decker' => 'Meepsicle',
            'Jinx' => 'Donkey',
            'TL' => 'TurdLicker'
        ];
        foreach($players as $name => $nickname) {
            Player::create([
                'name' => $name,
                'nickname' => $nickname
            ]);
        }
    }
}
