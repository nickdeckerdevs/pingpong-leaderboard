<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Player;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Validator;
use Redirect;

class PlayerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $players = Player::all();
        return view('player.index', compact('players'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('player.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            [
                'name' => Input::get('name'),
                'nickname' => Input::get('nickname')
            ],
            [
                'name' => 'required',
                'nickname' => 'required|unique:players'
            ]
        );
        
        if($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors()->all());
        }
        
        $newPlayer = Player::create([
            'name' => Input::get('name'),
            'nickname' => Input::get('nickname')
        ]);
        return Redirect::route('player.index')->with('message', $newPlayer->name.' has been created');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $edit = false;
        $player = Player::find($id);
        return view('player.show', compact('player', 'edit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $player = Player::find($id);
        $edit = true;
        return view('player.show', compact('player', 'edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request)
    {
        $input = array_except(Input::all(), '_method');
        
        $player = Player::find(Input::get('id'));
        $player->update($input);
        return Redirect::route('player.index')->with('message', $player->name.' has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        
    }
}
